- Resource 1 - Hjemmeside

    [http://www.infrateket.dk/4a/ap009-1.htm](http://www.infrateket.dk/4a/ap009-1.htm)

    - Gennemgang og forklaringer af hvert lag på dansk.
    - Grafisk billede af kommunikationen.

- Resource 2 - Hjemmeside

    [http://www.danskdatasikkerhed.dk/osi-model/](http://www.danskdatasikkerhed.dk/osi-model/)

    - Forståelige forklaringer til hvert lag.
    - Eksempler til hvert lag.

- Resource 3 - Video

    [https://www.youtube.com/watch?v=Ilk7UXzV_Qc](https://www.youtube.com/watch?v=Ilk7UXzV_Qc)

    - Grafiske beskrivelser om hvad hvert lag består af

- Resource 4 - Video

    [https://www.youtube.com/watch?v=HEEnLZV2wGI](https://www.youtube.com/watch?v=HEEnLZV2wGI)

    - God forklaring om hvad man bruger modellen til
    - Eksempler på fejl der kan forekomme i lagene