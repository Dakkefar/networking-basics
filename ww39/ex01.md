1 - IP adresser - [https://support.microsoft.com/en-gb/help/164015/understanding-tcp-ip-addressing-and-subnetting-basics](https://support.microsoft.com/en-gb/help/164015/understanding-tcp-ip-addressing-and-subnetting-basics)

- Fortæller om opbygningen af ip-adresser
- Fortæller om sammenhængen mellem ip-adresser og subnet masks

2 - Router - [https://us.norton.com/internetsecurity-iot-smarter-home-what-is-router.htm](https://us.norton.com/internetsecurity-iot-smarter-home-what-is-router.html)

- Forklarer de to typer af routere og hvordan en router fungerer.
- Forklarer forskellen på en router og et modem.

3 - Mac adresser - [https://www.geeksforgeeks.org/introduction-of-mac-address-in-computer-network/](https://www.geeksforgeeks.org/introduction-of-mac-address-in-computer-network/)

    - Forklarer mac-adresse formatet og hvor det kommer fra.
    - Forklarer om typerne af mac-adresser.
